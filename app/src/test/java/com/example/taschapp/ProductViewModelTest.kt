package com.example.taschapp

import com.example.taschapp.presentation.model.Product
import com.example.taschapp.presentation.viewmodel.ProductViewModel
import org.junit.Assert
import org.junit.Test

class ProductViewModelTest {

    private var mProductViewModel: ProductViewModel = ProductViewModel()

    private val product1: Product by lazy {
        Product().apply {
            id = 1
            name = "Bag 1"
            description = "description 1"
        }
    }

    private val product2: Product by lazy {
        Product().apply {
            id = 2
            name = "Bag 2"
            description = "description 2"
        }
    }

    @Test
    fun addProductToWishListTest(){
        mProductViewModel.addProductToWishList(Product())
        mProductViewModel.addProductToWishList(Product())
        mProductViewModel.addProductToWishList(Product())

        Assert.assertEquals(3, mProductViewModel.wishListProducts.size)
    }

    @Test
    fun removeProductFromWishListTest() {

        val productList = ArrayList<Product>()
        productList.add(product1)
        productList.add(product1)

        mProductViewModel.wishListProducts = productList
        //after insertion
        Assert.assertEquals(2, mProductViewModel.wishListProducts.size)

        mProductViewModel.removeProductFromWishList(product1)
        //after deletion
        Assert.assertEquals(1, mProductViewModel.wishListProducts.size)
    }

    @Test
    fun removeAllProductsFromWishList() {

        val productList = ArrayList<Product>()
        productList.add(Product())
        productList.add(Product())
        productList.add(Product())
        productList.add(Product())

        mProductViewModel.wishListProducts = productList
        //after insertion
        Assert.assertEquals(4, mProductViewModel.wishListProducts.size)

        mProductViewModel.removeAllProductsFromWishList()
        //after deletion
        Assert.assertEquals(0, mProductViewModel.wishListProducts.size)
    }

}