package com.example.taschapp.presentation.util

object AppConstants {

    //FLAGS
    const val PRODUCT_DETAILS_FLAG = 1

    //EXTRAS
    const val PRODUCT_ID_EXTRA = "PRODUCT_ID_EXTRA"
    const val ACTION_EXTRA = "ACTION_EXTRA"

    //BUNDLES
    const val PRODUCT_BUNDLE = "PRODUCT_BUNDLE"
    const val IS_PRODUCT_IN_WISH_LIST_BUNDLE = "IS_PRODUCT_IN_WISH_LIST_BUNDLE"

    //CONSTANTS
    const val ADD_TO_WISH_LIST = "ADD_TO_WISH_LIST"
    const val REMOVE_FROM_WISH_LIST = "REMOVE_FROM_WISH_LIST"
    const val OK = "OK"
    const val CANCEL = "Cancel"

}