package com.example.taschapp.presentation.di;

import com.example.taschapp.presentation.viewmodel.ProductViewModel;

import dagger.Component;

@Component(modules = ProductRepositoryModule.class)
public interface UseCaseComponent {
    void inject(ProductViewModel viewModel);
}
