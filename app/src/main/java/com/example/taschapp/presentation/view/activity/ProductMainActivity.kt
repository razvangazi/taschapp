package com.example.taschapp.presentation.view.activity

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.taschapp.R
import com.example.taschapp.presentation.model.Product
import com.example.taschapp.presentation.util.AppConstants
import com.example.taschapp.presentation.util.PRODUCT_LIST_TYPE
import com.example.taschapp.presentation.view.adapter.ProductCatalogueAdapter
import com.example.taschapp.presentation.view.adapter.ProductWishListAdapter
import com.example.taschapp.presentation.viewmodel.ProductViewModel
import io.reactivex.observers.DisposableSingleObserver


class ProductMainActivity : AppCompatActivity(), ProductCatalogueAdapter.ItemClickListener, ProductWishListAdapter.ItemClickListener {

    private lateinit var mProductViewModel: ProductViewModel
    private lateinit var mRvProductCatalogueList: RecyclerView
    private lateinit var mRvProductWishList: RecyclerView
    private lateinit var mTvTotalPriceLabel: TextView
    private lateinit var mTvSubTotalLabel: TextView
    private lateinit var mBtnProceedToCheckout: Button
    lateinit var mPbCatalogueLoadingSpinner: ProgressBar

    private lateinit var mProductCatalogueAdapter: ProductCatalogueAdapter
    private lateinit var mProductWishListAdapter: ProductWishListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_main)
        initComponents()
    }

    private fun initComponents() {
        initLayoutViews()
        initProductViewModel()
        initProductListAdapter()
        initProductCatalogueRecyclerView()
        initProductWishListRecyclerView()
        initProceedToCheckoutButtonClick()
        updateSubTotalViews()
        loadProductCatalogue()
    }

    private fun initLayoutViews() {
        mTvTotalPriceLabel = findViewById(R.id.tv_wish_list_total_price)
        mTvSubTotalLabel = findViewById(R.id.tv_wish_list_sub_total)
        mRvProductCatalogueList = findViewById(R.id.rv_product_list_catalogue)
        mRvProductWishList = findViewById(R.id.rv_product_list_wish_list)
        mBtnProceedToCheckout = findViewById(R.id.btn_proceed_to_checkout)
         mPbCatalogueLoadingSpinner = findViewById(R.id.pb_catalogue_loading_spinner)
    }

    private fun initProductCatalogueRecyclerView() {
        mRvProductCatalogueList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        mRvProductCatalogueList.itemAnimator = DefaultItemAnimator()
        mRvProductCatalogueList.adapter = mProductCatalogueAdapter
    }

    private fun initProductWishListRecyclerView() {
        mRvProductWishList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mRvProductWishList.itemAnimator = DefaultItemAnimator()
        mRvProductWishList.adapter = mProductWishListAdapter
    }

    private fun initProductViewModel() {
        mProductViewModel = ViewModelProvider(this).get(ProductViewModel::class.java)
        mProductViewModel.init()
    }

    private fun initProductListAdapter() {
        mProductCatalogueAdapter = ProductCatalogueAdapter(ArrayList(), this)
        mProductWishListAdapter = ProductWishListAdapter(ArrayList(), this)
    }

    private fun loadProductCatalogue() {
        showCatalogueLoading()
        mProductViewModel.getProductList(object : DisposableSingleObserver<List<Product>>() {
            override fun onSuccess(products: List<Product>) {
                mProductCatalogueAdapter.updateCatalogueProducts(products)
                hideCatalogueLoading()
            }

            override fun onError(e: Throwable) {
                hideCatalogueLoading()
            }

        })
    }

    override fun onItemClick(product: Product, productListType: PRODUCT_LIST_TYPE) {
        val intent = Intent(this, ProductDetailsActivity::class.java)
        intent.putExtra(AppConstants.PRODUCT_BUNDLE, product)
        when (productListType) {
            PRODUCT_LIST_TYPE.CATALOGUE -> intent.putExtra(AppConstants.IS_PRODUCT_IN_WISH_LIST_BUNDLE, false)
            PRODUCT_LIST_TYPE.WISHLIST -> intent.putExtra(AppConstants.IS_PRODUCT_IN_WISH_LIST_BUNDLE, true)
        }
        startActivityForResult(intent, AppConstants.PRODUCT_DETAILS_FLAG)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppConstants.PRODUCT_DETAILS_FLAG) {
            if (resultCode == Activity.RESULT_OK) {
                val product = data?.extras?.get(AppConstants.PRODUCT_ID_EXTRA) as Product
                when (data?.extras?.get(AppConstants.ACTION_EXTRA)) {
                    AppConstants.ADD_TO_WISH_LIST -> addProductToWishList(product)
                    AppConstants.REMOVE_FROM_WISH_LIST -> removeProductFromWishList(product)
                }
                updateSubTotalViews()
            }
        }
    }


    private fun initProceedToCheckoutButtonClick() {
        //Improvement: move Alert Dialog creation to a convenience method
        mBtnProceedToCheckout.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Are you sure you want to proceed to checkout?")
            builder.setPositiveButton(AppConstants.OK) { _, _ ->
                mProductViewModel.removeAllProductsFromWishList()
                mProductWishListAdapter.updateWishListProducts(mProductViewModel.wishListProducts)
                updateSubTotalViews()
            }
            builder.setNegativeButton(AppConstants.CANCEL) { _, _ -> }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }

    private fun addProductToWishList(product: Product) {
        mProductViewModel.addProductToWishList(product)
        mProductWishListAdapter.updateWishListProducts(mProductViewModel.wishListProducts)
    }

    private fun updateSubTotalViews() {
        val subTotal = mProductViewModel.subTotal
        mTvTotalPriceLabel.text = getString(R.string.product_wish_list_total_price, subTotal)
        mTvSubTotalLabel.text = getString(R.string.product_wish_list_sub_total_price, subTotal)
    }

    private fun removeProductFromWishList(product: Product) {
        mProductViewModel.removeProductFromWishList(product)
        mProductWishListAdapter.updateWishListProducts(mProductViewModel.wishListProducts)
    }

    private fun showCatalogueLoading() {
        mRvProductCatalogueList.visibility = View.GONE
        mPbCatalogueLoadingSpinner.visibility = View.VISIBLE
    }

    private fun hideCatalogueLoading() {
        mRvProductCatalogueList.visibility = View.VISIBLE
        mPbCatalogueLoadingSpinner.visibility = View.GONE
    }
}