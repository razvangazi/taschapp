package com.example.taschapp.presentation.view.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.taschapp.R
import com.example.taschapp.presentation.model.Product
import com.example.taschapp.presentation.util.PRODUCT_LIST_TYPE
import com.example.taschapp.presentation.util.ProductHelper


class ProductWishListAdapter(private var products: List<Product>, itemClickListener: ItemClickListener?) : RecyclerView.Adapter<ProductWishListAdapter.ProductViewHolder>() {
    private lateinit var mContext: Context
    private val mOnItemClickListener: ItemClickListener? = itemClickListener

    fun updateWishListProducts(products: List<Product>) {
        this.products = products
        notifyDataSetChanged()
    }

    inner class ProductViewHolder(var view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        init {
            view.setOnClickListener(this)
        }

        val mIvProductImage: ImageView = view.findViewById(R.id.iv_product_image)
        val mTvProductPrice: TextView = view.findViewById(R.id.tv_wish_list_product_price)
        val mTvProductName: TextView = view.findViewById(R.id.tv_wish_list_product_name)
        val mTvProductDescription: TextView = view.findViewById(R.id.tv_wish_list_product_description)
        val mTvProductColors: TextView = view.findViewById(R.id.tv_wish_list_product_colors)
        val mTvOutOfStockLabel: TextView = view.findViewById(R.id.tv_wish_list_out_of_stock)

        override fun onClick(v: View?) {
            mOnItemClickListener?.onItemClick(products[adapterPosition], PRODUCT_LIST_TYPE.WISHLIST)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        mContext = parent.context
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_product_wish_list_cell, parent, false)
        return ProductViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        if (position < products.size) {
            val currentProduct = products[position]
            val imgId = mContext.resources.getIdentifier(currentProduct.imageUrl, "drawable", mContext.packageName)
            holder.mIvProductImage.setImageResource(imgId)

            holder.mTvProductPrice.text = mContext.getString(R.string.product_price_value, currentProduct.price)
            holder.mTvProductName.text = currentProduct.name
            holder.mTvProductDescription.text = currentProduct.description
            holder.mTvProductColors.text = ProductHelper.getColors(currentProduct.colors)

            if (currentProduct.quantity > 0) {
                holder.mTvOutOfStockLabel.visibility = View.GONE
            } else {
                holder.mTvOutOfStockLabel.visibility = View.VISIBLE
            }
        }
    }

    // calling activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(product: Product, productListType: PRODUCT_LIST_TYPE)
    }

    override fun getItemCount(): Int {
        return products.size
    }
}
