package com.example.taschapp.presentation.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.taschapp.R
import com.example.taschapp.presentation.model.Product
import com.example.taschapp.presentation.util.AppConstants
import com.example.taschapp.presentation.util.ProductHelper

class ProductDetailsActivity : AppCompatActivity() {

    private var mProduct: Product? = null
    private var mIsProductInWishList: Boolean = false

    private lateinit var ivProductImage: ImageView
    private lateinit var tvProductPrice: TextView
    private lateinit var tvProductDescription: TextView
    private lateinit var tvProductColors: TextView

    private lateinit var tvProductSizeHeight: TextView
    private lateinit var tvProductSizeWidth: TextView
    private lateinit var tvProductSizeDepth: TextView
    private lateinit var btnAddToWishList: Button
    private lateinit var btnRemoveFromWishList: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        getBundleData()
        initComponents()
    }


    private fun initComponents() {
        setPageTitle()
        initLayoutViews()
        displayProductInformation()
    }

    private fun initLayoutViews() {
        ivProductImage = findViewById(R.id.tv_product_image)
        tvProductPrice = findViewById(R.id.tv_product_price)
        tvProductDescription = findViewById(R.id.tv_product_description)
        tvProductColors = findViewById(R.id.tv_product_colors_value)
        tvProductSizeHeight = findViewById(R.id.tv_product_height_value)
        tvProductSizeWidth = findViewById(R.id.tv_product_width_value)
        tvProductSizeDepth = findViewById(R.id.tv_product_depth_value)
        btnAddToWishList = findViewById(R.id.btn_add_product_to_wish_list)
        btnRemoveFromWishList = findViewById(R.id.btn_remove_product_to_wish_list)
    }

    private fun displayProductInformation() {
        initProductImage()
        initProductPrice()
        initProductDescription()
        initProductColors()
        initProductSize()
        initWishListButtons()
        initWishListButtonsClick()
    }

    private fun setPageTitle() {
        title = mProduct?.name
    }

    private fun getBundleData() {
        mProduct = intent.getParcelableExtra(AppConstants.PRODUCT_BUNDLE)
        mIsProductInWishList = intent.getBooleanExtra(AppConstants.IS_PRODUCT_IN_WISH_LIST_BUNDLE, false)
    }

    private fun initWishListButtons() {
        if (mIsProductInWishList) {
            btnRemoveFromWishList.visibility = View.VISIBLE
            btnAddToWishList.visibility = View.GONE
        } else {
            btnRemoveFromWishList.visibility = View.GONE
            btnAddToWishList.visibility = View.VISIBLE
        }
    }

    private fun initWishListButtonsClick() {
        btnAddToWishList.setOnClickListener {
            returnResultToWishList(AppConstants.ADD_TO_WISH_LIST)
        }
        btnRemoveFromWishList.setOnClickListener {
            returnResultToWishList(AppConstants.REMOVE_FROM_WISH_LIST)
        }
    }

    private fun returnResultToWishList(action: String) {
        val returnIntent = Intent()
        returnIntent.putExtra("ACTION_EXTRA", action)
        returnIntent.putExtra("PRODUCT_ID_EXTRA", mProduct)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    private fun initProductSize() {
        tvProductSizeHeight.text = mProduct?.size?.height
        tvProductSizeWidth.text = mProduct?.size?.width
        tvProductSizeDepth.text = mProduct?.size?.depth
    }

    private fun initProductColors() {
        //creates a string of colors separated by commas
        tvProductColors.text = ProductHelper.getColors(mProduct?.colors)
    }

    private fun initProductDescription() {
        tvProductDescription.text = mProduct?.description
    }

    private fun initProductPrice() {
        tvProductPrice.text = getString(R.string.product_price_value, mProduct?.price)
    }

    private fun initProductImage() {
        //improvement: add default picture in case the drawable is not found
        val imgId = resources.getIdentifier(mProduct?.imageUrl, "drawable", packageName)
        ivProductImage.setImageResource(imgId)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item)
    }
}