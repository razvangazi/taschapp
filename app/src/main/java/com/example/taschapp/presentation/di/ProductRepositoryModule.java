package com.example.taschapp.presentation.di;

import com.example.taschapp.data.ProductRepositoryImpl;
import com.example.taschapp.domain.repository.ProductRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class ProductRepositoryModule {
    @Provides
    ProductRepository productRepository(ProductRepositoryImpl productRepository) {
        return productRepository;
    }
}
