package com.example.taschapp.presentation.viewmodel;

import androidx.lifecycle.ViewModel;

import com.example.taschapp.domain.interactors.GetProductListUseCase;
import com.example.taschapp.presentation.di.DaggerUseCaseComponent;
import com.example.taschapp.presentation.di.UseCaseComponent;
import com.example.taschapp.presentation.model.Product;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;

public class ProductViewModel extends ViewModel {

    @Inject
    GetProductListUseCase mGetProductListUseCase;
    private List<Product> mWishListProducts = new ArrayList<>();

    public void init() {
        UseCaseComponent component = DaggerUseCaseComponent.create();
        component.inject(this);
    }

    public void getProductList(DisposableSingleObserver<List<Product>> disposableObserver) {
        mGetProductListUseCase.execute(disposableObserver, null, AndroidSchedulers.mainThread());
    }

    public List<Product> getWishListProducts() {
        return mWishListProducts;
    }

    public void setWishListProducts(List<Product> products) {
        mWishListProducts = products;
    }

    public void addProductToWishList(Product product) {
        //Improvement: avoid adding duplicate products
        mWishListProducts.add(product);
    }

    public void removeProductFromWishList(Product productToRemove) {
        for (Product currentProduct : mWishListProducts) {
            if (currentProduct.getId() == productToRemove.getId()) {
                mWishListProducts.remove(currentProduct);
                return;
            }
        }
    }

    public void removeAllProductsFromWishList(){
        mWishListProducts.clear();
    }

    public double getSubTotal() {
        double subTotal = 0;
        for (Product currentProduct : mWishListProducts) {
            subTotal += currentProduct.getPrice();
        }
        return subTotal;
    }
}
