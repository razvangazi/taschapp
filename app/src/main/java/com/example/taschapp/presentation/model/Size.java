package com.example.taschapp.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Size implements Parcelable {
    private String height;
    private String width;
    private String depth;

    public static final Parcelable.Creator<Size> CREATOR = new Parcelable.Creator<Size>() {
        @Override
        public Size createFromParcel(Parcel source) {
            return new Size(source);
        }

        @Override
        public Size[] newArray(int size) {
            return new Size[size];
        }
    };

    public Size() {
    }

    protected Size(Parcel in) {
        this.height = in.readString();
        this.width = in.readString();
        this.depth = in.readString();
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.height);
        dest.writeString(this.width);
        dest.writeString(this.depth);
    }

    @Override
    public String toString() {
        return "Size{" +
                "height='" + height + '\'' +
                ", width='" + width + '\'' +
                ", depth='" + depth + '\'' +
                '}';
    }
}
