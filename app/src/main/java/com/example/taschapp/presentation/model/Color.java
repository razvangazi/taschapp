package com.example.taschapp.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Color implements Parcelable {
    private String name;
    private String code;


    public static final Parcelable.Creator<Color> CREATOR = new Parcelable.Creator<Color>() {
        @Override
        public Color createFromParcel(Parcel source) {
            return new Color(source);
        }

        @Override
        public Color[] newArray(int size) {
            return new Color[size];
        }
    };

    public Color() {
    }

    protected Color(Parcel in) {
        this.name = in.readString();
        this.code = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.code);
    }

    @Override
    public String toString() {
        return "Color{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
