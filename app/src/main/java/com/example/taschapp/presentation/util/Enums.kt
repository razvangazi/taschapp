package com.example.taschapp.presentation.util

enum class PRODUCT_LIST_TYPE {
    CATALOGUE, WISHLIST
}