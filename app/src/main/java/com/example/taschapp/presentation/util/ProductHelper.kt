package com.example.taschapp.presentation.util

import com.example.taschapp.presentation.model.Color
import java.lang.StringBuilder

class ProductHelper {
    companion object {
        fun getColors(colors: List<Color>?): String {
            val colorsCombined = StringBuilder()
            colors?.forEachIndexed { index, color ->
                colorsCombined.append(color.name)
                //if its the last colors, dont append a comma
                if (index + 1 != (colors.size)) {
                    colorsCombined.append(", ")
                }
            }
            return colorsCombined.toString()
        }
    }
}