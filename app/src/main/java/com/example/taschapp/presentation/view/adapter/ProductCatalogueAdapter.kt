package com.example.taschapp.presentation.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.taschapp.R
import com.example.taschapp.presentation.model.Product
import com.example.taschapp.presentation.util.PRODUCT_LIST_TYPE


class ProductCatalogueAdapter(private var products: List<Product>, itemClickListener: ItemClickListener?) : RecyclerView.Adapter<ProductCatalogueAdapter.ProductViewHolder>() {
    private lateinit var mContext: Context
    private val mOnItemClickListener: ItemClickListener? = itemClickListener

    fun updateCatalogueProducts(products: List<Product>) {
        this.products = products
        notifyDataSetChanged()
    }

    inner class ProductViewHolder(var view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        init {
            view.setOnClickListener(this)
        }

        val mTvProductName: TextView = view.findViewById(R.id.tv_product_name)
        val mIvProductImage: ImageView = view.findViewById(R.id.iv_product_image)

        override fun onClick(v: View?) {
            mOnItemClickListener?.onItemClick(products[adapterPosition], PRODUCT_LIST_TYPE.CATALOGUE)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        mContext = parent.context
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_product_catalogue_cell, parent, false)
        return ProductViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        if (position < products.size) {
            holder.mTvProductName.text = products[position].name
            val imgId = mContext.resources.getIdentifier(products[position].imageUrl, "drawable", mContext.packageName)
            holder.mIvProductImage.setImageResource(imgId)
        }
    }

    // calling activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(product: Product, productListType: PRODUCT_LIST_TYPE)
    }

    override fun getItemCount(): Int {
        return products.size
    }
}
