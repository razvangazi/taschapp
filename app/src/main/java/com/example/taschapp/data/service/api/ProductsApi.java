package com.example.taschapp.data.service.api;

import com.example.taschapp.presentation.model.Product;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface ProductsApi {
    @GET("bins/6c3xa")
    Single<List<Product>> getProducts();
}
