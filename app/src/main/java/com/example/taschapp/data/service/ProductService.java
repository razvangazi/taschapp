package com.example.taschapp.data.service;

import com.example.taschapp.data.service.api.ProductsApi;
import com.example.taschapp.presentation.model.Product;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Retrofit;

public class ProductService {
    private static final String TAG = "ProductService";

    @Inject
    public ProductService() {
    }

    public Single<List<Product>> getProducts() {
        Retrofit retrofit = RetrofitHelper.getRetrofit();
        ProductsApi jsonPlaceholderApi = retrofit.create(ProductsApi.class);
        return jsonPlaceholderApi.getProducts();
    }

    //getProductDetails(int productId)

}
