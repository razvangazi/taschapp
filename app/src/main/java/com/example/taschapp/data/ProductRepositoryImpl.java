package com.example.taschapp.data;

import com.example.taschapp.data.service.ProductService;
import com.example.taschapp.domain.repository.ProductRepository;
import com.example.taschapp.presentation.model.Product;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class ProductRepositoryImpl implements ProductRepository {

    private ProductService mProductService;

    @Inject
    public ProductRepositoryImpl(ProductService productService) {
        mProductService = productService;
    }

    @Override
    public Single<List<Product>> getProducts() {
        return mProductService.getProducts();
    }
}
