package com.example.taschapp.domain.repository;

import com.example.taschapp.presentation.model.Product;

import java.util.List;

import io.reactivex.Single;

public interface ProductRepository {
    Single<List<Product>> getProducts();
}
