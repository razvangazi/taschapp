package com.example.taschapp.domain.interactors;

import com.example.taschapp.domain.interactors.base.BaseUseCase;
import com.example.taschapp.domain.repository.ProductRepository;
import com.example.taschapp.presentation.model.Product;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetProductListUseCase extends BaseUseCase<List<Product>, Void> {

    private ProductRepository mProductRepository;

    @Inject
    public GetProductListUseCase(ProductRepository productRepository) {
        mProductRepository = productRepository;
    }

    @Override
    protected Single<List<Product>> buildUseCaseObservable(Void aVoid) {
        return mProductRepository.getProducts();
    }
}
